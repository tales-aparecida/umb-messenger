"""Functionality related to UMB message sending."""
import contextlib
from copy import deepcopy
import json
import os

from cki_lib import certs
from cki_lib import misc
import stomp
from yaml import Loader
from yaml import load

from . import message_data
from . import templates
from .settings import LOGGER

DEFAULT_CONFIG_PATH = '/etc/cki/umb-config/umb.yml'
DEFAULT_SSL_PEM_PATH = '/etc/cki/umb/cki-bot-prod.pem'


class UMBClient:
    # pylint: disable=too-few-public-methods
    """Class for handling UMB communication."""

    ssl_pem_path = os.environ.get('SSL_PEM_PATH') or DEFAULT_SSL_PEM_PATH

    def __init__(self, message_type, config):
        """Initialize a client to report the pipeline."""
        self.message_type = message_type
        self.config = config
        self.brokers = self.config['brokers'].items()

    @contextlib.contextmanager
    def connect(self):
        """Safely connect to UMB."""
        connection = stomp.Connection(self.brokers, keepalive=True)
        try:
            connection.set_ssl(self.brokers,
                               key_file=self.ssl_pem_path,
                               cert_file=self.ssl_pem_path)
            connection.connect(wait=True)
            certs.update_certificate_metrics(self.ssl_pem_path)
            yield lambda message: self._send_message(connection, message)
        finally:
            with contextlib.suppress(Exception):
                connection.disconnect()

    def _send_message(self, connection, message):
        """Send a UMB message."""
        if 'error' in message or 'reason' in message:  # Something went wrong
            topic_config = f'{self.message_type}.error'
        else:
            topic_config = f'{self.message_type}.complete'

        try:
            topic = self.config[topic_config]
        except KeyError:
            # Not all topics may be configured, e.g. we're currently not
            # supporting error messages for 'ready_for_test'. This is a
            # feature, but we don't want to miss any real problems so log this.
            LOGGER.info('No topic configured for %s!', topic_config)
            return

        stringified_message = json.dumps(message)

        if misc.is_production():
            LOGGER.info('Sending message to %s', topic)
            connection.send(topic, stringified_message)
        else:
            LOGGER.info('Production mode would send %s to %s', message, topic)


def load_configs():
    """Load UMB configuration and return a dictionary representing it."""
    config_path = os.environ.get('UMB_CONFIG_PATH') or DEFAULT_CONFIG_PATH

    with open(config_path, encoding='utf-8') as config_file:
        return load(config_file, Loader=Loader)


def handle_message(umb_config, checkout, message_type):
    """
    Initialize UMB client, build and send the message and end the connection.

    Args:
        umb_config:   Dictionary of UMB communication.
        checkout:     KCIDB checkout data, as return by DataWarehouse API.
        message_type: Type of the message to send.
    """
    LOGGER.info('Gathering data for %s: %s', message_type, checkout.id)

    is_osci = message_type.startswith('osci_')

    try:
        if is_osci:
            client_config = umb_config[f'report_{checkout.tree_name}']
        else:
            client_config = umb_config['ready_for_test']
    except KeyError:
        # It's not an error if we only want to report some trees, but we want
        # to be able to track this information.
        LOGGER.info('No UMB settings for %s for %s!', message_type, checkout.id)
        return

    client = UMBClient(message_type, client_config)

    with client.connect() as send_message:
        # Get the template, but don't overwrite it!
        message = deepcopy(getattr(templates, message_type.upper()))

        if is_osci:
            for full_message in message_data.get_gating_data(message, checkout):
                send_message(full_message)
        else:
            full_message = message_data.get_ready_for_test_data(message, message_type, checkout)
            send_message(full_message)
