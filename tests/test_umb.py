"""Tests for umb_messenger.umb."""
# pylint: disable=no-self-use
import contextlib
import json
import os
import unittest
from unittest import mock

from cki_lib import messagequeue
from cki_lib import misc

from tests import fakes
from umb_messenger import umb

CHECKOUT = fakes.get_fake_checkout(actual_attrs={'tree_name': 'fedora'})


class TestHandleMessage(unittest.TestCase):
    """Tests for umb.handle_message()."""

    def test_unsupported(self):
        """Verify functionality for unsupported configs."""
        with self.assertLogs(umb.LOGGER, 'INFO') as log:
            umb.handle_message({}, CHECKOUT, 'unsupported')
            self.assertIn('No UMB settings', log.output[-1])

    @mock.patch('umb_messenger.message_data.get_gating_data')
    @mock.patch('stomp.Connection')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_osci_finished(self, mock_connection, mock_gating):
        """Sanity check for OSCI messages."""
        config = {'report_fedora': {'brokers': {'url': 'port'},
                                    'osci_finished.error': 'error topic',
                                    'osci_finished.complete': 'complete topic'}}

        message_good = {'message1': 'good'}
        message_bad = {'error': {'reason': 'this went wrong'}}
        mock_gating.return_value = [message_good, message_bad]

        umb.handle_message(config, CHECKOUT, 'osci_finished')
        mock_gating.assert_called_once()

        # Verify both messages and topics are handled correctly
        mock_connection.return_value.send.assert_has_calls([
            mock.call('complete topic', json.dumps(message_good)),
            mock.call('error topic', json.dumps(message_bad))
        ])

    @mock.patch('umb_messenger.message_data.get_gating_data')
    @mock.patch('stomp.Connection')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_osci_running(self, mock_connection, mock_gating):
        """Sanity check for OSCI messages."""
        config = {'report_fedora': {'brokers': {'url': 'port'},
                                    'osci_running.complete': 'complete topic'}}

        message = {'message1': 'good'}
        mock_gating.return_value = [message]

        umb.handle_message(config, CHECKOUT, 'osci_running')
        mock_gating.assert_called_once()

        mock_connection.return_value.send.assert_has_calls([
            mock.call('complete topic', json.dumps(message)),
        ])

    @mock.patch('umb_messenger.message_data.get_ready_for_test_data')
    @mock.patch('stomp.Connection')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_ready_for_test(self, mock_connection, mock_ready):
        """Sanity check for ready_for_test messages.

        The behavior is same for both pre_ and post_test messages so we don't
        need to test both.
        """
        config = {'ready_for_test': {'brokers': {'url': 'port'},
                                     'pre_test.complete': 'topic'}}

        message_good = {'message1': 'good'}
        mock_ready.return_value = message_good
        umb.handle_message(config, CHECKOUT, 'pre_test')

        # Verify "good" message and topic is handled correctly
        mock_connection.return_value.send.assert_called_with(
            'topic', json.dumps(message_good)
        )

        mock_ready.return_value = {'reason': 'this went wrong'}
        with self.assertLogs(umb.LOGGER, 'INFO') as log:
            umb.handle_message(config, CHECKOUT, 'pre_test')
            self.assertIn('No topic configured', log.output[-1])


@unittest.skipUnless(os.environ.get('RABBITMQ_HOST'), 'No RabbitMQ server configured')
class IntegrationTestStomp(unittest.TestCase):
    """Run integration tests.py."""

    def setUp(self):
        """Declare MessageQueue instance and exchanges."""
        self.rabbitmq_host = os.environ['RABBITMQ_HOST']
        self.rabbitmq_port = misc.get_env_int('RABBITMQ_PORT', 5672)
        self.rabbitmq_stomp_ssl_port = misc.get_env_int('RABBITMQ_PORT', 61614)
        self.rabbitmq_user = os.environ.get('RABBITMQ_USER', 'guest')
        self.rabbitmq_password = os.environ.get('RABBITMQ_PASSWORD', 'guest')
        self.rabbitmq_client_cert = os.environ.get('RABBITMQ_CLIENT_PRIVATE_KEY_AND_CERTIFICATE')

        self.messagequeue = messagequeue.MessageQueue(
            host=self.rabbitmq_host, port=self.rabbitmq_port,
            user=self.rabbitmq_user, password=self.rabbitmq_password,
            dlx_retry=False)

        self.queues = []
        self.exchanges = ['exchange.one']

        with self.messagequeue.connect() as channel:
            for exchange in self.exchanges:
                channel.exchange_declare(exchange)

    def tearDown(self):
        """Delete all queues and exchanges from the server."""
        with self.messagequeue.connect() as channel:
            for queue in self.queues:
                with contextlib.suppress(Exception):
                    channel.queue_delete(queue)
            for exchange in self.exchanges:
                with contextlib.suppress(Exception):
                    channel.exchange_delete(exchange)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_stomp_connect(self):
        """Test publishing and receiving a message."""
        callback = mock.Mock()
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.append(queue)

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback,
            queue_name=queue, inactivity_timeout=0.1)
        self.assertEqual(0, callback.call_count)

        client = umb.UMBClient('msg', {
            'brokers': {self.rabbitmq_host: self.rabbitmq_stomp_ssl_port},
            'msg.complete': 'queue.one',
        })
        client.ssl_pem_path = self.rabbitmq_client_cert
        with client.connect() as send_message:
            send_message({'foo': 'bar'})
        self.messagequeue.consume_messages(
            exchange, ['#'], callback,
            queue_name=queue, inactivity_timeout=0.1)

        self.assertEqual(1, callback.call_count)
        self.assertEqual(callback.mock_calls[0].kwargs['body'], {'foo': 'bar'})
